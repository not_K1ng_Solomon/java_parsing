package proect_java_5_avto;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

//import java.util.ArrayList;
//import java.util.List;

public class ListAuto {

	public static void main(String[] args) throws IOException {
		String FILENAME= "c:\\Users\\Shevket\\Desktop\\resultJson.json";// ����� ���� ������� ���� resultJson.json � ������� � ���� ���� �� ����� ��
		File file = new File(FILENAME);
		if (!file.exists()){
	        throw new FileNotFoundException(file.getName());
	    }
    	
    	for(int i=1;i<=100;i++){
    		Document doc = Jsoup.connect("https://www.avito.ru/rossiya/transport?p="+i).get();
    		Elements catalog_jsElements = doc.getElementsByAttributeValue("class", "title item-description-title");

    		catalog_jsElements.forEach(catalog_jsElement -> {	
    			Element aElement = catalog_jsElement.child(0);
    			String url =aElement.attr("href");
    			String title = aElement.attr("title");

    			//aElemenSystem.out.println(url+ '\n' + title);
    			JSONObject resultJson = new JSONObject();
    			resultJson.put("name", title);

    			resultJson.put("url", url);
    			System.out.println(resultJson.toString());
    			try {
    				update(file,resultJson.toString());
    			} catch (FileNotFoundException e) {
    				e.printStackTrace();
    			}
    		});
    		
    	}
    	long m = System.currentTimeMillis() / 1000;
    	System.out.println("����� ���������� : " + ((double) (System.currentTimeMillis() - m)));
    }
	public static void write(File file, String text) {
	    try {
	        if(!file.exists()){
	            file.createNewFile();
	        }
	        PrintWriter out = new PrintWriter(file.getAbsoluteFile());
	        
	        try {
	            out.print(text);
	        } finally {
	            out.close();
	        }
	    } catch(IOException e) {
	        throw new RuntimeException(e);
	    }
	}
	public static String read(File file) throws FileNotFoundException {
	    StringBuilder sb = new StringBuilder();
	 
	    try {
	        BufferedReader in = new BufferedReader(new FileReader( file.getAbsoluteFile()));
	        try {
	            String s;
	            while ((s = in.readLine()) != null) {
	                sb.append(s);
	                sb.append("\n");
	            }
	        } finally {
	            in.close();
	        }
	    } catch(IOException e) {
	        throw new RuntimeException(e);
	    }

	    return sb.toString();
	}
	/*private static void exists(String fileName) throws FileNotFoundException {
	    File file = new File(fileName);
	    if (!file.exists()){
	        throw new FileNotFoundException(file.getName());
	    }
	}*/
	public static void update(File file, String newText) throws FileNotFoundException {
	    StringBuilder sb = new StringBuilder();
	    String oldFile = read(file);
	    sb.append(oldFile);
	    sb.append(newText);
	    write(file, sb.toString());
	}
}